fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        panic!("Expected file name.");
    }

    let file_path = std::path::Path::new(&args[1]);

    let file_content = std::fs::read_to_string(&file_path)
                              .expect("Unable to open the file.");

    let mut input: Vec<&str> = Vec::new();
    for line in file_content.lines() {
        let mut line_split  = line.splitn(2, "//");
        let command = line_split.nth(0).unwrap();
        if !command.is_empty() {
            input.push(command);
        }
    }
    println!("{:?}", input);
}

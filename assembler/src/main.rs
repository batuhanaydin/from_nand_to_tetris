#![allow(dead_code)]

mod symbol_table;
use crate::parser::parser::prepare_input;
use std::{env, fs};

mod parser;
mod assembler;


fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        panic!("Write the file name");
    }
    let file_name = &args[1];
    let k = fs::read(file_name).expect("Something went wrong, failed to read the file.");
    let a = prepare_input(k);
    assembler::translate(a, &file_name);
    /* 
    for element in a {
        println!("{:?}", element);
        println!("{:?}", String::from_utf8(element).unwrap());
    } */
    //let file = fs::read(file_name).expect("Unable to read the file");

}

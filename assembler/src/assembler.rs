use std::fs::File;
use std::fs::OpenOptions;
use std::io::Write;
use ascii::AsciiChar;

use crate::parser::parser::Stream;

pub fn translate(input: Stream, file_name: &str) {
    let mut file = open_file(file_name).unwrap();
    for element in input {
        if *element.first().unwrap() == AsciiChar::At as u8 {
            let number = &element[1];
            let value = format!("{:#018b}", number);
            let result = value.split_at(2).1;
            writeln!(file, "{}", result).unwrap();
        } else {
            if element.contains(&(AsciiChar::Plus as u8)) 
            ||  element.contains(&(AsciiChar::Minus as u8))
            || element.contains(&(AsciiChar::Equal as u8)) {
                calculation_op(&element, &mut file);
            } else {
                jumping(&element, &mut file);
            }
        }
    }
}

fn calculation_op(input: &[u8], file: &mut File) {

    let mut result = String::with_capacity(16);
    result.push_str("111");
    let mut splitted = input.split(|x| *x == AsciiChar::Equal);

    let dest = splitted.next().unwrap();
    let mut dest_str = String::with_capacity(3);
    match *dest {
        [77] => dest_str.push_str("001"),
        [68] => dest_str.push_str("010"),
        [68, 77] => dest_str.push_str("011"),
        [65] => dest_str.push_str("100"),
        [65, 77] => dest_str.push_str("101"),
        [65, 68] => dest_str.push_str("110"),
        [65, 68, 77] => dest_str.push_str("111"),
        _ => dest_str.push_str("000")
    }

    let comp = splitted.next().unwrap();

    let is_m = comp.contains(&77);
    let mut a_value = "0";
    if is_m == true {
        a_value = "1";
    }
    // 68:D, 43:+, 45:-, 65:A, 77:M, &:38, |:124
    let mut comp_str = String::with_capacity(6);
    match *comp {
        [48] => comp_str.push_str("101010"),
        [49] => comp_str.push_str("111111"),
        [45, 49] => comp_str.push_str("111010"),
        [68] => comp_str.push_str("001100"),
        [65] | [77] => comp_str.push_str("110000"),
        [33, 68] => comp_str.push_str("001101"),
        [33, 65] | [33, 77] => comp_str.push_str("110001"),
        [45, 68] => comp_str.push_str("001111"),
        [45, 65] | [45, 77] => comp_str.push_str("110011"),
        [68, 43, 49] => comp_str.push_str("011111"),
        [65, 43, 49] | [77, 43, 49] => comp_str.push_str("110111"),
        [68, 45, 49] => comp_str.push_str("001110"),
        [65, 45, 49] | [77, 45, 49] => comp_str.push_str("110010"),
        [68, 43, 65] | [68, 43, 77] => comp_str.push_str("000010"),
        [68, 45, 65] | [68, 45, 77] => comp_str.push_str("010011"),
        [65, 45, 68] | [77, 45, 68] => comp_str.push_str("000111"),
        [68, 38, 65] | [68, 38, 77] => comp_str.push_str("000000"),
        [68, 124, 65] | [68, 124, 77] => comp_str.push_str("010101"),
        _ => ()
    }

    result.push_str(a_value);
    result.push_str(&comp_str);
    result.push_str(&dest_str);
    result.push_str("000");
    writeln!(file, "{}", result).unwrap();
}

fn jumping(input: &[u8], file: &mut File) {
    let mut result = String::with_capacity(16);
    result.push_str("1110");
    let mut splitted = input.split(|x| *x == AsciiChar::Semicolon);

    let dest = splitted.next().unwrap();
    match *dest {
        [68] => result.push_str("001100"),
        [48] => result.push_str("101010"),
        _ => ()
    }

    result.push_str("000");
    let jump = splitted.next().unwrap();
    match *jump {
        [74, 71, 84] => result.push_str("001"),
        [74, 69, 81] => result.push_str("010"),
        [74, 71, 69] => result.push_str("011"),
        [74, 76, 84] => result.push_str("100"),
        [74, 78, 69] => result.push_str("101"),
        [74, 76, 69] => result.push_str("110"),
        [74, 77, 80] => result.push_str("111"),
        _ => ()
    }

    writeln!(file, "{}", result).unwrap();
}

fn open_file(file_name: &str) -> Result<File, std::io::Error> {
    let filename_splitted: Vec<&str> = file_name.split(".").collect();
    let newfilename = format!("{}.hack", filename_splitted[0]);
    OpenOptions::new()
                    .write(true)
                    .append(true)
                    .create(true)
                    .open(newfilename)
                                        
}
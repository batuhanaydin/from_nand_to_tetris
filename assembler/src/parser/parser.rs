use ascii::AsciiChar;

pub type Stream = Vec<Vec<u8>>;

pub fn prepare_input(input: Vec<u8>) -> Stream {
    let mut stream = split_by_lines(&input);
    remove_comments_and_space(&mut stream);
    crate::symbol_table::get_symbol_table(&mut stream);
    stream
}

fn split_by_lines(input: &[u8]) -> Stream {
    let mut stream = Stream::new();
    let mut new_vec: Vec<u8> = Vec::new();
    for element in input {
        if *element == AsciiChar::LineFeed {
            stream.push(new_vec);
            new_vec = Vec::new();
            continue;
        } 
        new_vec.push(*element);
    }
    stream
}


fn remove_comments_and_space(input: &mut Stream) {
    input.retain(|x| x.len() > 1);
    input.retain(|x| x[0] != AsciiChar::Slash && x[1] != AsciiChar::Slash);
    for element in input.iter_mut() {
        element.retain(|x| *x != AsciiChar::CarriageReturn && *x != AsciiChar::Space);
        if let Some(index) = element.windows(2)
            .position(|pair| pair[0] == AsciiChar::Slash && pair[1] == AsciiChar::Slash) {
            element.truncate(index);
        }
    }
    input.retain(|x| x.len() > 0);
}
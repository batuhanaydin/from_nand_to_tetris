use std::{collections::HashMap, ops::Index};

use crate::parser::parser::Stream;

pub type SymbolTable = HashMap<String, usize>;


pub fn get_symbol_table(stream: &mut Stream)  {
    let mut table = initialize_symbol_table();
    get_symbols(&mut table, &stream);
    replace_symbols(&mut table, stream);
}

fn initialize_symbol_table() -> SymbolTable {
    let mut table: SymbolTable = SymbolTable::new();
    for i in 0..16 {
        let key = format!("R{}", i);
        table.insert(key, i);
    }
    table.insert(String::from("SP"), 0);
    table.insert(String::from("LCL"), 1);
    table.insert(String::from("ARG"), 2);
    table.insert(String::from("THIS"), 3);
    table.insert(String::from("THAT"), 4);
    table.insert(String::from("SCREEN"), 16384);
    table.insert(String::from("KBD"), 23476);
    table
}

fn get_symbols(table: &mut SymbolTable, stream: &Stream)
{
    for (index, element) in stream.iter().enumerate() {
        if element.starts_with(&[40u8]) && element.ends_with(&[41u8]) {
            let key = String::from_utf8_lossy(&element[1..element.len()-1]).into_owned();
            table.insert(key, index + 1);
        }
    }
}

fn replace_symbols(table: &mut SymbolTable, stream: &mut Stream)
{
    let mut index: usize = 16;
    for element in stream.iter_mut() {
        if element.starts_with(&[64u8]) {
            let characters = String::from_utf8_lossy(&element[1..]).into_owned();
            match characters.parse::<i32>() {
                Ok(_) => continue,
                Err(_) => {
                    if !table.contains_key(&characters) {
                        table.insert(characters.clone(), index);
                        index += 1;
                    }
                    let mut new_vec: Vec<u8> = vec![64];
                    new_vec.push(table[&characters] as u8);
                    std::mem::replace(element, new_vec);
                }
            }; 
        }
    }

    stream.retain(|element| !(element.starts_with(&[40u8]) 
    && element.ends_with(&[41u8])))
}